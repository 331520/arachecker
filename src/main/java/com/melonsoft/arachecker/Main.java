/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.melonsoft.arachecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.ini4j.Wini;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ArbuzovA
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     * @throws java.io.FileNotFoundException
     */
    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException, FileNotFoundException, IOException {
        logger.debug("");
        logger.debug("Start");
        LocalDateTime start = LocalDateTime.now();
        //ini file
        File iniFile = new File("config.ini").getAbsoluteFile();
        logger.info("ini : " + iniFile.toString());
        Wini ini = new Wini(new File(iniFile.getAbsolutePath()));
        Integer webdriverwait = ini.get("main_sec", "webdriverwait", int.class);
        String jobView = ini.get("main_sec", "main_url");
        String jobList = ini.get("main_sec", "jobList");

        //classes
        SupportFactory sf = new SupportFactory();
        NewWebDriver newWebDriver = new NewWebDriver();
        
        //create new driver
        WebDriver driver = newWebDriver.driver();
        WebDriverWait wait = new WebDriverWait(driver, webdriverwait);

        Integer jobCounter = 0;
        logger.debug("Try to open Siebel");
        driver.get(jobView);

        //lobs list to check
        List<String> jbList = new ArrayList<>();
        jbList = sf.JobList(jobList);

        //build empty table
        HTMLTableBuilder htmlBuilder = null;
        
        //Do we create a short report?
        if (ini.get("main_sec", "smalltable").equals("y")) {
            htmlBuilder = new HTMLTableBuilder(null, true, 2, 11);
            htmlBuilder.addTableHeader("#", "Job Name", "Job Id", "Job State", "Repeat Id", "Repeat Status", "Prev Repeat Id", "Prev Status", "Prev Completion Text", "Prev Start Date", "TimeFrom");

        } else {
            htmlBuilder = new HTMLTableBuilder(null, true, 2, 13);
            htmlBuilder.addTableHeader("#", "Job Name", "Job Id", "Job State", "Repeat #", "Repeat Id", "Repeat Status", "Prev Repeat #", "Prev Repeat Id", "Prev Status", "Prev Completion Text", "Prev Start Date", "TimeFrom");
        }

        //main loop. Checking jobs state
        for (String job : jbList) {
            try {
                System.out.println("");
                System.out.println("" + job);
                logger.debug("Process " + job);

                //are list loaded to applet?
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@title='Jobs:Query']"))).click();
                logger.debug("Search button pressed");

                //Activate field
                WebElement jobNameField = driver.findElement(By.id("1_s_1_l_Job_Name"));
                jobNameField.click();

                //put the process name
                driver.switchTo().activeElement().sendKeys(job);
                driver.switchTo().activeElement().sendKeys(Keys.RETURN);

                //process state
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@title='Jobs:Query']"))); // all data loaded to table?
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("1_s_1_l_StatusIcon")));
                String stateFieldText = driver.findElement(By.id("1_s_1_l_StatusIcon")).getText();
                System.out.println("Job state is : " + stateFieldText);
                logger.debug("Job state is : " + stateFieldText);

                //process id
                String jobId = driver.findElement(By.id("1_s_1_l_Id")).getText(); //1_s_1_l_Id
                System.out.println("Job Id : " + jobId);

                //go to repeating instances
                WebElement repInstTab = driver.findElement(By.linkText("Repeating Instances"));
                repInstTab.click();

                //awailting load page title="Request Tasks:Query"
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@title='Request Tasks:Query']"))); // all data loaded to table?
                Thread.sleep(200);

                //check repeat state
                String currentRepeatNumber = driver.findElement(By.id("1_s_2_l_Repeat_Number")).getText();
                System.out.println("Current Repeat Number : " + currentRepeatNumber);

                String currentRepeatId = driver.findElement(By.id("1_s_2_l_Id")).getText();
                System.out.println("Current Repeat Id : " + currentRepeatId);

                String currentRepeatStatus = driver.findElement(By.id("1_s_2_l_Status_Displayed")).getText();
                System.out.println("Current Repeat Status : " + currentRepeatStatus);

                logger.debug("Repeating Instance was read");

                //check prev. repeat state
                String prevRepeatNumber = "n/a";
                String prevRepeatId = "n/a";
                String prevRepeatStatus = "n/a";
                String prevRepeatCompletionCode = "n/a";
                String prevRepeatCompletion_Text = "n/a";
                String prevRepeatActualStartDate = "n/a";
                String timeFrom = "n/a";
                try {
                    logger.debug("Try to get prev. state");
                    prevRepeatNumber = driver.findElement(By.id("2_s_2_l_Repeat_Number")).getText();
                    System.out.println("Prev Repeat Number : " + prevRepeatNumber);

                    prevRepeatId = driver.findElement(By.id("2_s_2_l_Id")).getText();
                    System.out.println("Prev Repeat Id : " + prevRepeatId);

                    prevRepeatStatus = driver.findElement(By.id("2_s_2_l_Status_Displayed")).getText();
                    System.out.println("Prev Repeat Satatus : " + prevRepeatStatus);

                    prevRepeatCompletionCode = driver.findElement(By.id("2_s_2_l_Completion_Code")).getText();
                    System.out.println("Prev Repeat Completion Code : " + prevRepeatCompletionCode);

                    prevRepeatCompletion_Text = driver.findElement(By.id("2_s_2_l_Completion_Text")).getText();
                    System.out.println("Prev Repeat Completion Text : " + prevRepeatCompletion_Text);
                    //if something horrible happens!!!
                    if (prevRepeatCompletion_Text.length() > 3) {
                        prevRepeatCompletion_Text = "<mark>" + prevRepeatCompletion_Text + "</mark>";
                        logger.debug("WARNING : " + prevRepeatCompletion_Text);
                    }

                    prevRepeatActualStartDate = driver.findElement(By.id("2_s_2_l_Actual_Start_Date")).getText();
                    System.out.println("Prev Repeat Actual Start Date : " + prevRepeatActualStartDate);
                    logger.debug("Prev. Repeating Instance was read");
                    
                    timeFrom = sf.getTimeFrom(prevRepeatActualStartDate);
                    System.out.println("Time From was calculated");
                    logger.debug("Time From was calculated");

                } catch (Exception e) {
                    logger.debug("No prev repeated instance" + e.getLocalizedMessage());
                    System.out.println("No previous repeating instance : " + e.getLocalizedMessage());
                }

                jobCounter++; // how many jobs was processed 

                // create new table's row
                if (ini.get("main_sec", "smalltable").equals("y")) {
                    htmlBuilder.addRowValues(jobCounter.toString(), job, jobId, stateFieldText, currentRepeatId, currentRepeatStatus, prevRepeatId, prevRepeatStatus, prevRepeatCompletion_Text, prevRepeatActualStartDate, timeFrom);
                } else {
                    htmlBuilder.addRowValues(jobCounter.toString(), job, jobId, stateFieldText, currentRepeatNumber, currentRepeatId, currentRepeatStatus, prevRepeatId, prevRepeatStatus, prevRepeatCompletion_Text, prevRepeatActualStartDate, timeFrom);
                }

                //how many?
                System.out.println(jobCounter + " lines processed");
                System.out.println(jbList.size() - jobCounter + " lines left to process");

                //break;//for debug. delete it
            } catch (Exception e) {
                logger.debug("Error processing job's data : " + e.getLocalizedMessage());
                System.out.println("Error processing job's data : " + e.getLocalizedMessage());
            }
        }
        logger.debug("Create table");
        String table = htmlBuilder.build();
        sf.saveTable(table);
        //when leaving, turn off the light, please!
        driver.close();
        driver.quit();
        //How long we work?
        LocalDateTime stop = LocalDateTime.now();
        Duration duration = Duration.between(stop, start);
        long diff = Math.abs(duration.toMinutes());
        long diffSeconds = Math.abs(duration.toMillis() / 1000);
        System.out.println("Completed in " + diff + " minutes");
        logger.debug("Completed in " + diffSeconds + " seconds");
        System.exit(0); //that's all
    }
}
